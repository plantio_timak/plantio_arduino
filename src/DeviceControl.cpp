#include "DeviceControl.h"
#include <Time.h>
#include <TimeLib.h>

#define BUFF_MAX 256

/////////////////////////////////////////////////
/// \brief Constructor DeviceControl
/////////////////////////////////////////////////
DeviceControl::DeviceControl()
{
}

/////////////////////////////////////////////////
/// \brief Destructor DeviceControl
/////////////////////////////////////////////////
DeviceControl::~DeviceControl()
{
}

/////////////////////////////////////////////////
/// \brief Function to read the temperature and the humidity from DHT sensor
/////////////////////////////////////////////////
void DeviceControl::readRoomStats(BH1750 *lightMeter, DHT *dht, Room *room)
{
    room->humidity = dht->readHumidity();
    room->temperature = dht->readTemperature();
    room->light = lightMeter->readLightLevel();
}

/////////////////////////////////////////////////
/// \brief Function to read the light in lux from BH1750 sensor
/////////////////////////////////////////////////
float DeviceControl::readLightSensor(BH1750 *lightMeter)
{
    return lightMeter->readLightLevel();
}

/////////////////////////////////////////////////
/// \brief Function to read the water level
/////////////////////////////////////////////////
int DeviceControl::readWaterLevel(uint8_t pin)
{
    return analogRead(pin); //Read data from analog pin and store it to resval variable
}

/////////////////////////////////////////////////
/// \brief Function to read the plant humidity in [%]
/////////////////////////////////////////////////
int DeviceControl::readPlantHumidity(uint8_t PIN)
{
    int humidity = analogRead(PIN);
	//humidity = constrain(humidity,270,1023);	//Keep the ranges!
	humidity = map(humidity,1023,200,0,100);	//Map value : 400 will be 100 and 1023 will be 0
    return humidity;
}

/////////////////////////////////////////////////
/// \brief Function to read the plant temperature in [°C]
/////////////////////////////////////////////////
float DeviceControl::readPlantTemperature(DallasTemperature *sensors)
{
    sensors->requestTemperatures();
    return sensors->getTempCByIndex(0);
}

/////////////////////////////////////////////////
/// \brief Function to switch the apliance ON/OFF or OPEN/CLOSED
/////////////////////////////////////////////////
int DeviceControl::switchAppliance(uint8_t pin, uint8_t val)
{
    if (digitalRead(pin) != val)
    {
        digitalWrite(pin, val);
        return 0;
    }
    return -1;
}

/////////////////////////////////////////////////
/// \brief Function to create Json document with necessary info about the plant
/////////////////////////////////////////////////
void DeviceControl::createJsonPlant(DynamicJsonDocument& doc, const char* measurement, int plantId, struct Plant *plant)
 {
	doc["measurement"] = measurement;
	doc["time"] = getTime();
	doc["fields"]["plant_id"] = plantId; 
   	doc["fields"]["temperature"] = plant->temperature;
   	doc["fields"]["humidity"] = plant->humidity;
}

/////////////////////////////////////////////////
/// \brief Function to create Json document with necessary info about the room
/////////////////////////////////////////////////
void DeviceControl::createJsonRoom(DynamicJsonDocument& doc, const char* measurement, int roomId, struct Room *room)
{    	
    doc["measurement"] = measurement;
	doc["time"] = getTime();
	doc["fields"]["room_id"] = roomId; 
   	doc["fields"]["temperature"] = room->temperature;
   	doc["fields"]["humidity"] = room->humidity;
    doc["fields"]["light_intensity"] = room->light;
    doc["fields"]["water_level"] = room->waterLevel;

}

/////////////////////////////////////////////////
/// \brief Function to send data from plant measurement to NodeMCU
/////////////////////////////////////////////////
void DeviceControl::sendPlantMeasurementToNodeMCU(SoftwareSerial &nodemcu, int ID, struct Plant *plant)
{
    DynamicJsonDocument doc(BUFF_MAX);
    createJsonPlant(doc, "plant_state", ID, plant);    
    JsonObject root = doc.as<JsonObject>(); 
    serializeJson(doc, nodemcu);
    doc.clear();
    root.clear();
}

/////////////////////////////////////////////////
/// \brief Function to send data from room measurement to NodeMCU
/////////////////////////////////////////////////
void DeviceControl::sendRoomMeasurementToNodeMCU(SoftwareSerial &nodemcu, int ID, struct Room *room)
{
    DynamicJsonDocument doc(BUFF_MAX);
    createJsonRoom(doc, "room_state", ID, room);    
    JsonObject root = doc.as<JsonObject>(); 
    serializeJson(doc, nodemcu);
    doc.clear();
    root.clear();    
}

void DeviceControl::printPlantInfo(struct Plant *plant)
{
    Serial.print("Plant Humidity [%]: ");
    Serial.println(plant->humidity);
    Serial.print("Plant [°C]: ");
    Serial.println(plant->temperature);    
}

void DeviceControl::printRoomInfo(struct Room *room)
{
    //Print temp and humidity values to serial monitor
    Serial.print("Room Humidity [%]: ");
    Serial.print(room->humidity);
    Serial.print(" Temp [°C]: ");
    Serial.println (room->temperature);

    Serial.print("Room Light [lx]: ");
    Serial.println(room->light);
}

float DeviceControl::getAverage(int size, float *array)
{
    float sum = 0;
    
    for (size_t i = 0; i < size; i++)
    {
        sum += array[i];
    }
    float average = sum / (float) size;
    return average;
}

char* DeviceControl::getTime() 
{
    static char buff[BUFF_MAX];
    struct ts t;

    DS3231_get(&t);

    // display current time
    snprintf(buff, BUFF_MAX, "%d.%02d.%02d %02d:%02d:%02d", t.year,
            t.mon, t.mday, t.hour, t.min, t.sec);

    return buff;
}