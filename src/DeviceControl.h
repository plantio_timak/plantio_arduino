#ifndef DEVICE_CONTROL_H
#define DEVICE_CONTROL_H

#include <Arduino.h>
#include "DHT.h"
#include <ArduinoJson.h>
#include <BH1750.h>
#include <DallasTemperature.h>
#include <SoftwareSerial.h>
#include <ds3231.h>

class WateringInfo
{
public:    
    WateringInfo()
    {
        wateringMode = 'T';
        wateringCycleHours = 24;
        minHumidity = 50;
    };

    char wateringMode;
    int wateringCycleHours;
    int minHumidity;
};

class Plant
{
public:

    Plant(){humidity = 0; temperature = 0;};
    int humidity;
    float temperature;
};

class Room
{
public:
    Room(){humidity = 0; temperature = 0; light = 0; waterLevel = "";}
    float humidity;
    float temperature;
    float light;
    char* waterLevel;
};

class DeviceControl
{
public:
    DeviceControl();
    ~DeviceControl();

    int readWaterLevel(uint8_t pin);
    void readRoomStats(BH1750 *lightMeter, DHT *dht, Room *room);
    int readPlantHumidity(uint8_t PIN);
    float readLightSensor(BH1750 *lightMeter);
    int switchAppliance(uint8_t pin, uint8_t val);
    float readPlantTemperature(DallasTemperature *sensors);

    void createJsonPlant(DynamicJsonDocument& doc, const char* measurement, int plantId, struct Plant *plant);
    void createJsonRoom(DynamicJsonDocument& doc, const char* measurement, int roomId, struct Room *room);

    void sendPlantMeasurementToNodeMCU(SoftwareSerial &nodemcu, int ID, struct Plant *plant);
    void sendRoomMeasurementToNodeMCU(SoftwareSerial &nodemcu, int ID, struct Room *room);

    void printPlantInfo(struct Plant *plant);
    void printRoomInfo(struct Room *room);

    float getAverage(int size, float *array);
    char* getTime();

private:

};

#endif