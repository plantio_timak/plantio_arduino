#include <SoftwareSerial.h>
#include <Wire.h>
#include <DallasTemperature.h>
#include "DHT.h"
#include <BH1750.h>
#include <ArduinoJson.h>
#include <Time.h>
#include <OneWire.h>
#include <pt.h>
#include "DeviceControl.h"
#include <ds3231.h>
#include <RTClib.h>

//SENSORS PINS
#define DHTPIN 2  
#define DHTYPE DHT21
#define SOLENOID_1 5
#define SOLENOID_2 4
#define WATER_PUMP 3
#define WIFI_MODUL_RX 10  // 6=Rx
#define WIFI_MODUL_TX 11  // 7=Tx
#define PLANT_TEMEPRATURE_1 8
#define PLANT_TEMEPRATURE_2 9
#define SENSE_1 A7        // Soil Sensor input at Analog PIN A7
#define SENSE_2 A8        // Soil Sensor input at Analog PIN A7
#define WATER_LEVEL A0    // Water Level Sensor input at Analog PIN A0


#define MEASUREMENTS 15
#define BUFF_MAX 256

RTC_DS3231 rtc;

BH1750 lightMeter;
OneWire plantTemperatureSensor_1(PLANT_TEMEPRATURE_1);
OneWire plantTemperatureSensor_2(PLANT_TEMEPRATURE_2);

DallasTemperature sensors_1(&plantTemperatureSensor_1);
DallasTemperature sensors_2(&plantTemperatureSensor_2);

DHT dht(DHTPIN, DHTYPE);
SoftwareSerial nodemcu(WIFI_MODUL_RX, WIFI_MODUL_TX); //Initialise Arduino to NodeMCU 
DeviceControl device;

bool isWaterInTank = false;

static WateringInfo plantInfo_1;
static WateringInfo plantInfo_2; 

struct ts t; //date time structure

// Declare  protothreads
static struct pt pt1, pt2, pt3, pt4;

static unsigned long pt1Wait = 0;

static Room room;
static Plant plant_1;
static Plant plant_2;

uint32_t timeTriger_1 = 4294967295;
uint32_t timeTriger_2 = 4294967295;

// 2nd protothread function 
// to water the plants
static int protothreadWateringPlants(struct pt *pt)
{
  static unsigned long lastTimeCheck = 0;
  PT_BEGIN(pt);
  while(1)
  {
    lastTimeCheck = millis();
    PT_WAIT_UNTIL(pt, millis() - lastTimeCheck > 5000);

    DateTime now = rtc.now();
    uint32_t timeNow = now.unixtime();

    if(isWaterInTank)
    {
        if (plantInfo_1.wateringMode == 'A')
        {
            if(plantInfo_1.minHumidity >  device.readPlantHumidity(SENSE_1))
            {
                device.switchAppliance(SOLENOID_1, LOW);
                device.switchAppliance(WATER_PUMP, LOW);
            }
        }
        else if (plantInfo_1.wateringMode == 'T')
        {
            if (timeNow > timeTriger_1)            
            {
                timeTriger_1 = timeNow + ((uint32_t) 3600 * (uint32_t) plantInfo_1.wateringCycleHours);
                device.switchAppliance(SOLENOID_1, LOW);
                device.switchAppliance(WATER_PUMP, LOW);
            } 
        }

        if (plantInfo_2.wateringMode == 'A')
        {
            if(plantInfo_2.minHumidity > device.readPlantHumidity(SENSE_2))
            {
                device.switchAppliance(SOLENOID_2, LOW);
                device.switchAppliance(WATER_PUMP, LOW);
            }
        }
        else if (plantInfo_2.wateringMode == 'T')
        {
            if (timeNow > timeTriger_2)
            {
                timeTriger_2 = timeNow + ((uint32_t) 3600 * (uint32_t) plantInfo_1.wateringCycleHours);
                device.switchAppliance(SOLENOID_1, LOW);
                device.switchAppliance(WATER_PUMP, LOW);
            } 
        }
    }
    lastTimeCheck = millis();

    PT_WAIT_UNTIL(pt, millis() - lastTimeCheck > 3000);
    device.switchAppliance(SOLENOID_1, HIGH);
    device.switchAppliance(SOLENOID_2, HIGH);
    device.switchAppliance(WATER_PUMP, HIGH);
  }
  PT_END(pt);
}

// 3nd protothread function to write number from
// Serial to water the plants
static int protothreadWaterLevel(struct pt *pt)
{
  static unsigned long lastTimeCheck = 0;
  PT_BEGIN(pt);
  while(1)
  {
    lastTimeCheck = millis();
    PT_WAIT_UNTIL(pt, millis() - lastTimeCheck > 1000);
    int waterLevel = device.readWaterLevel(WATER_LEVEL);


    if (waterLevel > 500)
        isWaterInTank = true;
    else    
        isWaterInTank = false;

    if(!isWaterInTank)
    {
        device.switchAppliance(SOLENOID_1, HIGH);
        device.switchAppliance(SOLENOID_2, HIGH);
        device.switchAppliance(WATER_PUMP, HIGH);
    }
  }
  PT_END(pt);
}

// 4nd protothread function
static int protothreadWateringMode(struct pt *pt)
{
  static unsigned long lastTimeCheck = 0;
  PT_BEGIN(pt);
  while(1)
  {
    lastTimeCheck = millis();
    PT_WAIT_UNTIL(pt, millis() - lastTimeCheck > 100);

    DynamicJsonDocument doc(BUFF_MAX);
    auto error = deserializeJson(doc, nodemcu);
    if (error) {
        //Serial.print(F("deserializeJson() failed with code "));
        //Serial.println(error.c_str());
        return;
    }
    JsonObject obj = doc.as<JsonObject>();
    char* mode = doc["mode"];
    const char* deviceID = doc["deviceID"];

    char jsonStr[256];
    serializeJson(doc, jsonStr);
    Serial.println(jsonStr);

    DateTime now = rtc.now();
    uint32_t timeNow = now.unixtime();

    if(strcmp(deviceID, "3") == 0)
    {
        if(strcasecmp(mode, "INTERACTIVE") == 0)
        {
            plantInfo_1.wateringMode = 'A';
            plantInfo_1.minHumidity = atoi(doc["min_soil_humidity"]);
            plantInfo_1.wateringCycleHours = -1;
        }
        else if(strcasecmp(mode, "TIME") == 0)
        {
            plantInfo_1.wateringMode =  'T';
            plantInfo_1.minHumidity = -1;
            plantInfo_1.wateringCycleHours = atoi(doc["time"]);

            timeTriger_1 = timeNow + ( (uint32_t) 3600 * (uint32_t) plantInfo_1.wateringCycleHours);
        }
    }
    else if(strcmp(deviceID, "4") == 0)
    {
        if(strcasecmp(mode, "INTERACTIVE") == 0)
        {
            plantInfo_2.wateringMode = 'A';
            plantInfo_2.minHumidity = atoi(doc["min_soil_humidity"]);
            plantInfo_2.wateringCycleHours = -1;
        }
        else if(strcasecmp(mode, "TIME") == 0)
        {
            plantInfo_2.wateringMode =  'T';
            plantInfo_2.minHumidity = -1;
            plantInfo_2.wateringCycleHours = atoi(doc["time"]);
            
            timeTriger_2 = timeNow + ((uint32_t) 3600 * (uint32_t) plantInfo_2.wateringCycleHours);
        }
    }

    doc.clear();
    obj.clear();
  }
  PT_END(pt);
}

// Thread to send measurements from sensors to NodeMCU
static int protothreadSensorsData(struct pt *pt)
{
    static unsigned long lastTimeMeasurement = 0;

    static int count = 0;
    static float roomHumdityArray[MEASUREMENTS] = {};
    static float roomTemperatureArray[MEASUREMENTS] = {};
    static float roomLightArray[MEASUREMENTS] = {};

    static float plantHumdityArray_1[MEASUREMENTS] = {};
    static float plantTemperatureArray_1[MEASUREMENTS] = {};

    static float plantHumdityArray_2[MEASUREMENTS] = {};
    static float plantTemperatureArray_2[MEASUREMENTS] = {};

    PT_BEGIN(pt);
    while(1) 
    {
        lastTimeMeasurement = millis();
        PT_WAIT_UNTIL(pt, millis() - lastTimeMeasurement > pt1Wait);

        device.readRoomStats(&lightMeter, &dht, &room);
        //device.printRoomInfo(&room);

        roomHumdityArray[count] = room.humidity;
        roomTemperatureArray[count] = room.temperature;
        roomLightArray[count] = room.light;
        
        plant_1.humidity = device.readPlantHumidity(SENSE_1);
        plant_1.temperature = device.readPlantTemperature(&sensors_1);
        //device.printPlantInfo(&plant_1);
        plantHumdityArray_1[count] = plant_1.humidity;
        plantTemperatureArray_1[count] = plant_1.temperature;

        plant_2.humidity =  device.readPlantHumidity(SENSE_2);
        plant_2.temperature = device.readPlantTemperature(&sensors_2);
        //device.printPlantInfo(&plant_2);
        plantHumdityArray_2[count] = plant_2.humidity;
        plantTemperatureArray_2[count] = plant_2.temperature;
        
        if(count == MEASUREMENTS -1)
        {
            room.humidity = device.getAverage(MEASUREMENTS, roomHumdityArray);
            room.temperature = device.getAverage(MEASUREMENTS, roomTemperatureArray);
            room.light = device.getAverage(MEASUREMENTS, roomLightArray);
            if(isWaterInTank)
                room.waterLevel = "true";
            else            
                room.waterLevel = "false";
                        
            device.sendRoomMeasurementToNodeMCU(nodemcu, 2, &room);
            Serial.println("MQTT SENT");

            lastTimeMeasurement = millis();
            PT_WAIT_UNTIL(pt, millis() - lastTimeMeasurement > 7500);

            plant_1.humidity = device.getAverage(MEASUREMENTS, plantHumdityArray_1);
            plant_1.temperature = device.getAverage(MEASUREMENTS, plantTemperatureArray_1);
            device.sendPlantMeasurementToNodeMCU(nodemcu, 3, &plant_1);
            Serial.println("MQTT SENT");

            lastTimeMeasurement = millis();
            PT_WAIT_UNTIL(pt, millis() - lastTimeMeasurement > 7500);

            plant_2.humidity = device.getAverage(MEASUREMENTS, plantHumdityArray_2);
            plant_2.temperature = device.getAverage(MEASUREMENTS, plantTemperatureArray_2);
            device.sendPlantMeasurementToNodeMCU(nodemcu, 4, &plant_2);
            Serial.println("MQTT SENT");
            pt1Wait = 15000;
            count = -1;
        }
        else
            pt1Wait = 30000;

        count++;
    }
    PT_END(pt);
}

void setup() 
{
    Serial.begin(9600);
    Wire.begin();
    nodemcu.begin(9600);

    while (!Serial); // Waiting for Serial Monitor
    

    sensors_1.begin(); 
    sensors_2.begin(); 

    dht.begin();
    
    pinMode(SOLENOID_1, OUTPUT);
    pinMode(SOLENOID_2, OUTPUT);
    pinMode(WATER_PUMP, OUTPUT);

    if (lightMeter.begin())
        Serial.println(F("BH1750 initialised"));
    else
        Serial.println(F("Error initialising BH1750"));

    DS3231_init(DS3231_INTCN); //register the ds3231 (DS3231_INTCN is the default address of ds3231, this is set by macro for no performance loss)

    device.switchAppliance(SOLENOID_1, HIGH);
    device.switchAppliance(SOLENOID_2, HIGH);
    device.switchAppliance(WATER_PUMP, HIGH);

    PT_INIT(&pt1);
    PT_INIT(&pt2);
    PT_INIT(&pt3);
    PT_INIT(&pt4);
}

void loop()
{
    protothreadSensorsData(&pt1);
    protothreadWateringPlants(&pt2);
    protothreadWaterLevel(&pt3);
    protothreadWateringMode(&pt4);
}

/*void scanI2C()
{
    byte error, address; //variable for error and I2C address
    int nDevices;

    Serial.println("Scanning...");
    nDevices = 0;
    for (address = 1; address < 127; address++)
    {
        // The i2c_scanner uses the return value of
        // the Write.endTransmisstion to see if
        // a device did acknowledge to the address.
        Wire.beginTransmission(address);
        error = Wire.endTransmission();

        if (error == 0)
        {
            Serial.print("I2C device found at address 0x");
            if (address < 16)
                Serial.print("0");
            Serial.print(address, HEX);
            Serial.println("  !");
            nDevices++;
        }
        else if (error == 4)
        {
            Serial.print("Unknown error at address 0x");
            if (address < 16)
                Serial.print("0");
            Serial.println(address, HEX);
        }
    }
    if (nDevices == 0)
        Serial.println("No I2C devices found\n");
    else
        Serial.println("done\n");
}*/