//NodeMCU code
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>

#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

#define AIO_SERVER      "147.175.121.239"
#define AIO_SERVERPORT  3000
#define AIO_USERNAME    ""
#define AIO_KEY         ""

//D6 = Rx & D5 = Tx
SoftwareSerial nodemcu(D2, D1);

WiFiClient client;

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

Adafruit_MQTT_Publish plantState = Adafruit_MQTT_Publish(&mqtt, "topic/plantio/plant_state");
Adafruit_MQTT_Publish roomState = Adafruit_MQTT_Publish(&mqtt, "topic/plantio/room_state");

Adafruit_MQTT_Subscribe wateringMode = Adafruit_MQTT_Subscribe(&mqtt, "topic/plantio/water_mode");

void MQTTConnect();

void setup() {
  // Initialize Serial port
  Serial.begin(9600);
  delay(10);

  nodemcu.begin(9600);
  while (!Serial) continue;  
  
  WiFiManager wifiManager;
  wifiManager.autoConnect("AutoConnectAP");
        
  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&wateringMode);
}

void loop() 
{
  if (WiFi.status() == WL_DISCONNECTED) 
  {
      Serial.println("Lost WiFi Connection! Restarting...");
      ESP.restart();      
  }

  MQTTConnect();

  DynamicJsonDocument docSub(256);
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) 
  {
    if(subscription == &wateringMode) 
    {  
      Serial.print((char *)wateringMode.lastread);

      DeserializationError error = deserializeJson(docSub, (char *)wateringMode.lastread);
      // Test if parsing succeeds.
      if (error) 
      {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        return;
      }      
      serializeJson(docSub, nodemcu);
    }
  }    
  docSub.clear();  

  DynamicJsonDocument doc(256);
  DeserializationError error = deserializeJson(doc, nodemcu);
  if (error) {
      //Serial.print(F("deserializeJson() failed with code "));
      //Serial.println(error.c_str());
      return;
  }
  JsonObject obj = doc.as<JsonObject>();
  const char* measurement = doc["measurement"];
  
  char jsonStr[256];
  serializeJson(doc, jsonStr);

  if(strcmp(measurement, "plant_state")  == 0)
  { 
      Serial.println(jsonStr); 
      if (! plantState.publish(jsonStr))      
        Serial.println(F("Failed"));      
      else      
        Serial.println(F("OK!"));      
  }
  else if(strcmp(measurement, "room_state") == 0)
  {
      Serial.println(jsonStr);
      if (! roomState.publish(jsonStr))
        Serial.println(F("Failed"));
      else
        Serial.println(F("OK!"));
  }
  
  doc.clear();
  obj.clear();
}

void MQTTConnect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
